import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/toast.dart';

class OtpVerificationScreen extends StatefulWidget {

String phone;

OtpVerificationScreen(this.phone);


  @override
  _OtpVerificationScreenState createState() => _OtpVerificationScreenState();

}

class _OtpVerificationScreenState extends State<OtpVerificationScreen> {

    TextEditingController otpController = new TextEditingController();

    bool progressBarNotLoading = true;

    FirebaseAuth auth = FirebaseAuth.instance;

    String _verificationId;


    @override
  void initState() {
    super.initState();

    getOTP()
    .whenComplete((){

      print('getOTP called');

    });

  }

  Future getOTP() async{

    print("getOTP()");

      await auth.verifyPhoneNumber(
        phoneNumber: '${widget.phone}',
        verificationCompleted: (PhoneAuthCredential credential) {
          // ANDROID ONLY!

          // Sign the user in (or link) with the auto-generated credential
          //await auth.signInWithCredential(credential);

          login();

        },
        verificationFailed: (FirebaseAuthException e) {

          print("Error found verificationFailed >>> code ${e.code}");
          print("Error found verificationFailed >>> $e");

          if(e.code == "invalid-phone-number"){
            Toast.show("Votre numéro de téléphone est invalide.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
            return;
          }


        },
        codeSent: (String verificationId, int resendToken)async {

          _verificationId = verificationId;
        },
        codeAutoRetrievalTimeout: (String verificationId) {

        },
      );

    }

    verifyOTP(){
      print("verifyOTP()");
      showProgress();
      // Update the UI - wait for the user to enter the SMS code
      String smsCode = otpController.text.trim();
      if(smsCode.isEmpty){
        Toast.show("Entrez svp le OTP", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        return;
      }
        // Create a PhoneAuthCredential with the code
        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(verificationId: _verificationId, smsCode: smsCode);

        // Sign the user in (or link) with the credential
        auth.signInWithCredential(phoneAuthCredential)
        .then((UserCredential value){
          hideProgress();

          print("OTP correct");

          Toast.show("Votre code est correct.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

          login();

        }).catchError((onError){

          hideProgress();

          print('Error found $onError');

          if(onError is FirebaseAuthException){

            if(onError.code == "invalid-verification-code"){
              Toast.show("Votre code est invalide.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
              return;
            }

            if(onError.code == "session-expired"){
              Toast.show("Votre code a expiré.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
              return;
            }

          }

        });


    }

    login(){

      print("logging user");

      /// Nous allons envoyer à notre serveur le numéro de téléphone et l'indicatif afin de savoir si c'est un
      /// nouvel utilisateur ou c'est un ancien
      /// Si c'est un nouvel utilisateur il faut le rédiriger vers l'écran d'inscription
      /// Si c'est un ancien utilisateur il faut enregistrer dans la base de donnée du téléphone ses informations récupérer du serveur et le rédiriger vers l'écran principal

    }


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: new SingleChildScrollView(
        child: new Column(
            children:[


              Container(
                margin: EdgeInsets.only(top:200.0, left:30.0),
                alignment: Alignment.topLeft,
                child: Text("Saisir le code de SMS envoyé au", style: TextStyle(fontSize: 20.0, color: Colors.black),),
              ),
              Container(
                margin: EdgeInsets.only(left:30.0),
                alignment: Alignment.topLeft,
                child: Text("${widget.phone}", style: TextStyle(fontSize: 20.0, color: Colors.black,fontWeight: FontWeight.bold,)),
              ),


              SizedBox(height: 150.0,),

              Container(
                  margin: EdgeInsets.only(left:30.0, right:30.0),
                  child:TextField(
                    controller: otpController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        hintText: "000000"
                    ),
                    style: TextStyle(fontSize: 40.0, ),
                  )
              ),

              Container(
                margin: EdgeInsets.only(left: 30.0, right:30.0, top: 30.0, bottom: 20.0),
                alignment: Alignment.topLeft,
                child: new InkWell(
                  onTap: (){
                    getOTP();
                  },
                  child: Text("Renvoyer le code",
                    style: TextStyle(fontSize: 17.0, color: Color(0xff264f8c)),
                  ),
                ),
              ),

              Container(
                margin:  EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 50.0),
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: (){

                    final String codeOtp = otpController.text;

                    print("Donnée saisi: $codeOtp");


                    verifyOTP();


                  },
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                  color: Theme.of(context).primaryColor,
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Text("Suivant", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black)),
                ),
              ),

              Offstage(
                offstage: progressBarNotLoading,
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                  backgroundColor: Theme.of(context).primaryColor,
                ),

              ),

              SizedBox(height:50.0)

            ]
        ),
      )
    );
  }
                                
                  void showProgress() {
                
                    print("showProgress called");
                
                    setState(() {
                      progressBarNotLoading = false;
                    });
                  }
                
                    void hideProgress() {
                    
                    setState(() {
                      progressBarNotLoading = true;
                    });
                
                  }
                
               /*   void verifyOTP() {


                    
                    Timer.periodic(Duration(minutes: 5), (timer) {



                            
                      Navigator.push(context, MaterialPageRoute(builder: (context){
                          return new HomeScreen();
                      }));

                     });


                  }*/
}