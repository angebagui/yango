import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
     // backgroundColor: Colors.grey,
     appBar: AppBar(),
      body: new Stack(
        children: <Widget>[
          buildMapWidget(),
          buildModalWidget()
        ],
      )
    );

  }

  Widget buildMapWidget(){

    return new Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: new Stack(
       children:[
         Image.asset('images/map.jpg',
          fit:BoxFit.cover,  
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
        ),

        Container(
          color: Colors.black.withOpacity(0.4),
        )
       ]
     ),
    );
  }

  Widget buildModalWidget(){
    return new Column(
        children:[
          Expanded(
            child: Container(
            color: Colors.transparent,
          )
          ),

          Container(
             
             decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0)
                  )
             ),
             child: Column(
                children: <Widget>[

                    SizedBox(height: 10.0,),
                    Container(
                      child: Image.asset("images/bonhomme.png", height: 180.0,),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(top: 50.0, left: 25.0, bottom: 20.0),
                      child: Text(
                        "Données de l'utilisateur", 
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25.0
                          ),
                        ),
                    ),

                    Container(
                      alignment: Alignment.topLeft ,
                      margin: EdgeInsets.only(left:25.0, right: 25.0),
                      child: Text(
                        "Yango envoie aux développeurs des données sur l'utilisation. Elles sont nécessaires à de nombreuses fonctions utiles de l'application.",
                        
                        style: TextStyle(fontSize: 20.0),

                        ),
                    ),

                    Container(
                        alignment: Alignment.topLeft ,
                        margin: EdgeInsets.only(left:25.0, right: 25.0, top: 20.0),
                      child: Text(
                        "Vous pouvez savoir comment nous traitons les données et comment refuser leur collecte sur la page Yango et données utilisateur.", 
                        style: TextStyle(fontSize: 20.0),
                        ),
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(top:20.0, bottom:20.0),
                      margin: EdgeInsets.all(25.0),
                    
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(20.0)
                        ),
                      alignment: Alignment.center,
                      child: Text(
                        "Continuer",
                        style: TextStyle(fontSize: 18.0),
                      ),
                    )



                ],
        ),
          )
        ]
      ) ;
  }


}