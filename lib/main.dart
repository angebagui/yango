import 'package:flutter/material.dart';
import 'package:yango/PhoneScreen.dart';
// Import the firebase_core plugin
import 'package:firebase_core/firebase_core.dart';


//  ... + 3 = 5

//  x  + 3 = 5    X =2; Y = 5
// y = x + 3    x = 1; y = 4 /  x = 2; y = 5 /   x=3; y= 6

// P(x) = x+3 // Polynome
// f(x) = x + 3  // Fonction f(1)  = 1 + 3
// main(int x) => intier (void)
// f (1)
// f (X: 1 )


void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Yango',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: Color(0xffffdd39),
        primaryColorDark: Color(0xffffcc00),
        accentColor: Colors.red,
        fontFamily: "YandexSansText",
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: new PhoneScreen(),
    );
  }
}




