import 'package:flutter/material.dart';
import 'package:yango/OtpVerificationScreen.dart';

class PhoneScreen extends StatefulWidget {
  @override
  _PhoneScreenState createState() => _PhoneScreenState();
}

class _PhoneScreenState extends State<PhoneScreen> {

  TextEditingController phoneNumberController = new TextEditingController(text: "+225");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:new SingleChildScrollView(
        child:  Column(
            children: [

              Container(
                margin: EdgeInsets.only(top:200.0, left:30.0),
                alignment: Alignment.topLeft,
                child: Text("Saisir le numéro de Téléphone", style: TextStyle(fontSize: 20.0, color: Colors.black),),
              ),



              Container(
                  margin: EdgeInsets.only(left:30.0, right:30.0),
                  child:TextField(
                    controller: phoneNumberController,
                    keyboardType: TextInputType.number,
                    style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  )
              ),

              Container(
                margin: EdgeInsets.only(left: 30.0, right:30.0, top: 100.0),
                child: Text("En continuant, je reconnais accepter les conditions de Contrat de licence Yandex.Taxi et Conditions d'utilisation et j'accepte que mes données soient traitées conformement aux conditions de Politique de confidentialité",
                  style: TextStyle(fontSize: 17.0, color: Colors.grey[500]),
                ),
              ),



              Container(
                margin:  EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 50.0),
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: (){

                    if(phoneNumberController.text.isEmpty){
                      return;
                    }

                    String phoneNumber = "";

                    if(phoneNumberController.text.contains('+')){
                      phoneNumber = phoneNumberController.text;
                    }else{
                      phoneNumber =  "+${phoneNumberController.text}";
                    }

                    print("Donnée saisi: $phoneNumber");

                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return new OtpVerificationScreen(phoneNumber);
                    }));

                  },
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                  color: Theme.of(context).primaryColor,
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: Text("Suivant", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black)),
                ),
              )

            ]
        ),
      )
    );
  }
}